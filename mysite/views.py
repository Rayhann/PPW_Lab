from django.shortcuts import render,redirect
from .models import Schedules
from mysite.forms import HomeForm


response = {}
def tes(request):
    return render(request, "Test.html")
def tes1(request):
    return render(request, "Test1.html")
def tes2(request):
    return render(request, "Test2.html")
def tes3(request):
	form = HomeForm(request.POST)
	if(request.method == 'POST' and form.is_valid()):
		model = Schedules(
			day = request.POST['day'],
			date = request.POST['date'],
			time = request.POST['time'],
			name = request.POST['name'],
			place = request.POST['place'],
			categories = request.POST['categories']
		)
		model.save()
	return redirect("rendertes3")
def rendertes3(request):
	response['form'] = HomeForm()
	return render(request, "Test3.html", response)
def apagitu(request):
	response['apagitu2'] = Schedules.objects.all().values()
	return render(request, 'jadwalku.html', response)
def hapus(request):
	response={}
	response['apagitu2'] = Schedules.objects.all().delete()
	return redirect('apagitu')
                
          