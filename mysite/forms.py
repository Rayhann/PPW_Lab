from django import forms


class HomeForm(forms.Form):
	name = forms.CharField(label='Nama Kegiatan', required=True, max_length=30, widget=forms.TextInput())
	day = forms.CharField(label='Hari', required=True, max_length=30, widget=forms.TextInput())
	date = forms.DateField(label="Tanggal", widget=forms.DateInput(attrs={'type':'date', 'class':'col-12', 'style':'margin-bottom:3vw'}))
	time = forms.TimeField(label='Waktu', required=True, widget=forms.TimeInput(attrs={'type': 'time', 'class':'col-12', 'style':'margin-bottom:3vw'}))
	place = forms.CharField(label='Tempat Kegiatan', required=True, max_length=30, widget=forms.TextInput())
	categories = forms.CharField(label='Kategori', required=True, max_length=30, widget=forms.TextInput())
