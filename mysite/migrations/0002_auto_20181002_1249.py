# Generated by Django 2.1.1 on 2018-10-02 05:49

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('mysite', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='schedules',
            name='categories',
            field=models.CharField(default='', max_length=30),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='schedules',
            name='place',
            field=models.CharField(default='', max_length=30),
            preserve_default=False,
        ),
    ]
