from django.conf.urls import url

from .views import tes, tes1, tes2, apagitu, tes3, rendertes3, hapus
urlpatterns = [
 url(r'^Test$', tes, name='tes'),
 url(r'^Test1$', tes1, name='tes1'),
 url(r'^Test2$', tes2, name='tes2'),
 url(r'^Test3$', tes3, name='tes3'),
 url(r'^bukaForm$', rendertes3, name='rendertes3'),
 url(r'^apagitu$', apagitu, name='apagitu'),
 url(r'^hapus$', hapus, name='hapus')
]
